# Installation

*if you have any request regarding the project please contact the author at <sangsoic@protonmail.com>*

Installation of the configuration files is made via the phony targets in the makefile.

`make [TARGET] ...`

Here is a list of all possible targets:

* bash
* vim
* nvim
* i3
* xresource
* all
