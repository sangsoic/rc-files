.PHONY: all nvim vim bash i3 xresource

bash : 
	mkdir ~/.config
	cp -vi .bashrc .config/bash ~/

vim : 
	cp -vi .vimrc ~/

nvim : 
	mkdir ~/.config
	cp -rvi .config/nvim/ ~/.config/

i3 : 
	mkdir ~/.config
	cp -rvi .config/i3/ ~/.config/

xresource : 
	cp -vi .Xdefaults ~/

all : 
	for setup in bash vim nvim xresource i3; do \
		$(MAKE) $$setup; \
	done
